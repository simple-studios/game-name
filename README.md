# GameName

Developed with Unreal Engine 4

### Authors

| Role | Name|
| --------------|------------------------------------------------|
|Game Designer 	|[Семён Корелин](https://t.me//suhtovi) |
|Programmer	| [Валентин Хакимов](https://t.me//mr_lampochka) |
|3D-Artist	| [Сутыркин Владислав](https://t.me//vsutyrkin) |
|QA-Engineer	| [Иванов Георгий](https://t.me//VVikiboy) |